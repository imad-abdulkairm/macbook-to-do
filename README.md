# MacOS initial setup

In this file there will be the list of all the things you need to install on a fresh mac.

## Applications
the `colored` part is the name of the app in brew cask
1. Install [brew](brew.sh)

2. run:
``` sh
  brew install --cask appcleaner qbittorrent visual-studio-code discord slack steam monitorcontrol brave-browser signal parsec firefox

  brew install node yarn tldr
```

3. app store apps
  - WhatsApp
  - WireGuard
  - Microsoft ToDo

 4. other
  - Final Cut Pro x
  - RawDigger (purchased)
  - Raw Right Away


## Remap Backspace Command (with caps)
```sh
hidutil property --set '{"UserKeyMapping":[{"HIDKeyboardModifierMappingSrc":0x700000039,"HIDKeyboardModifierMappingDst":0x70000002A}, {"HIDKeyboardModifierMappingSrc":0x70000002A,"HIDKeyboardModifierMappingDst":0x7000000E5}]}'
```

## Zsh Improvements
```sh
git clone https://github.com/zsh-users/zsh-autosuggestions $ZSH_CUSTOM/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/

# then add this to .zshrc
plugins=(... zsh-autosuggestions zsh-syntax-highlighting)
```

## extensions
- uBlock Origin
- Vim
- DirectLinks

## Setup Screenshots
```sh
# jpg is much smaller than png screen shots
defaults write com.apple.screencapture type jpg
killall SystemUIServer
```