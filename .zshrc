# Don't copy paste this file, instead append it or update your file
ZSH_THEME="robbyrussell"

plugins=(zsh-autosuggestions fast-syntax-highlighting)

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export PATH="/usr/local/sbin:$PATH"